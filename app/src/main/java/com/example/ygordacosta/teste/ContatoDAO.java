package com.example.ygordacosta.teste;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class ContatoDAO extends SQLiteOpenHelper{

    private static final String DATABASE = "Contato";
    private static final int VERSAO = 1;
/*
    public ContatoDAO(Context context) {
        super(context, DATABASE, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase sq) {
        String ddl = "CREATE TABLE Contato " +
                "(" +
                " id  INTEGER PRIMARY KEY ,"+
                "nome TEXT NOT NULL ," +
                " telefone TEXT   ," +
                "celular TEXT ," +

                "endereco TEXT "+
                ") ;";

         sq.execSQL(ddl);

    }
       public  void salvar (Contato contato){
        ContentValues values = new ContentValues();
        //values.put("imagem",contato.getImagem());
        values.put("nome", contato.getNome() );
        values.put("telefone", contato.getTelefone() );
        values.put("celular", contato.getCelular() );
        values.put("endereco", contato.getEndereço() );


        if(contato.getId() == 0){
            getWritableDatabase().insert("Contato",null,values);
        }else{
            getWritableDatabase().update("Contato",values,"id ="+contato.getId(),null);

        }

    }
  // Perguntar.
        public List<Contato>getLista() {
            List<Contato> lista = new ArrayList<>();

            String[] colunas = {"id","nome","telefone","celular","endereco"} ;

            Cursor cursor = getWritableDatabase().query("Contato",colunas,null,null,null,null,null);


            while(cursor.moveToNext()){

                Contato contato = new Contato();
                contato.setId(cursor.getInt(0));
                contato.setNome(cursor.getString(1));
                contato.setEndereço(cursor.getString(2));
                contato.setCelular(cursor.getString(3));
               // contato.setImagem(cursor.getString(4));
                contato.setTelefone(cursor.getString(4));

                lista.add(contato);
            }
            return lista;
        }




    @Override
    public void onUpgrade(SQLiteDatabase sq, int i, int i1) {
        String ddl = "DROP TABLES IF EXISTS Contato ; " ;
        sq.execSQL(ddl);
        this.onCreate(sq);


    }
    */
    public ContatoDAO(Context context){
       super(context,DATABASE,null,VERSAO);

    }

    @Override
    public void onCreate(SQLiteDatabase sq) {
        String ddl =  "CREATE TABLE Contato"+
                "( id INTEGER PRIMARY KEY , " +
                "nome TEXT NOT NULL , " +
                "imagem TEXT , " +
                "endereco TEXT , " +
                "celular TEXT ," +
                "telefone TEXT) ; " ;

        sq.execSQL(ddl);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String ddl = "DROP TABLE IF EXISTS Contato ;";
        sqLiteDatabase.execSQL(ddl);
        this.onCreate(sqLiteDatabase);

    }

    public void salvar (Contato contato){
        ContentValues values = new ContentValues();
        values.put("nome", contato.getNome());
        values.put("imagem", contato.getImagem());
        values.put("endereco", contato.getEndereço());
        values.put("celular", contato.getCelular());
        values.put("telefone", contato.getTelefone());

        if (contato.getId()== 0){
            getWritableDatabase().insert("Contato", null,values);
        }else{
            getWritableDatabase().update("Contato",values, "id ="+contato.getId(),null);

        }

    }
        public List<Contato>getLista(){
            List<Contato>Lista = new ArrayList<>();
            String colunas[] = {"id","nome","imagem","endereco","celular","telefone"};

            Cursor cursor = getWritableDatabase().query("Contato", colunas,null,null,null,null,null);
           while(cursor.moveToNext()){
               Contato contato = new Contato();

               contato.setId(cursor.getInt(0));
               contato.setNome(cursor.getString(1));
               contato.setImagem(cursor.getString(2));
               contato.setTelefone(cursor.getString(3));
               contato.setCelular(cursor.getString(4));
               contato.setEndereço(cursor.getString(5));





               Lista.add(contato);

           }
           return Lista;
        }




}
