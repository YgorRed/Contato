package com.example.ygordacosta.teste;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetalheActivity extends AppCompatActivity {
    private Contato contato;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe);

        Intent intent = getIntent();


        contato = (Contato) intent.getSerializableExtra(MainActivity.CONTATO);

        if(contato != null){

            TextView nome = findViewById(R.id.Nome);
            TextView telefone = findViewById(R.id.Telefone);
            TextView celularcontato = findViewById(R.id.Celular);
            TextView endereço = findViewById(R.id.Endereço);
            ImageView imagemV = findViewById(R.id.foto);

            Bitmap imagem = BitmapFactory.decodeFile(contato.getImagem()) ;




            nome.setText(contato.getNome());
            telefone.setText(contato.getTelefone());
            endereço.setText(contato.getEndereço());
            celularcontato.setText(contato.getCelular());
            imagemV.setImageBitmap(imagem);



        }




    }
}
